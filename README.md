

![picture](https://static.wixstatic.com/media/fd8c45_1d96faef4915464d9c0449dedb380e88~mv2.png/v1/fill/w_288,h_72,al_c,q_85,usm_0.66_1.00_0.01/packk-logo.webp){:height="50%" width="50%"}


# Desafio Packk Mobile
-----
### OBJETIVOS
O teste consiste em se desenvolver um app nativo iOS ou Android, dessa forma, deverá ser implementado em Swift ou Kotlin.

#### PRIMÁRIO:
Desenvolver um app no qual será uma listagem de posts, como o Facebook. Receber os dados do endpoint GET: https://jsonplaceholder.typicode.com/posts e mostrar
ao usuário a listagem com o título / body .
#### SECUNDÁRIO:
Permitir que o usuário vizualize os comentários do post escolhido e que também consiga publicar algo. A publicação (POST) deve ser salva localmente. Utilizar o endpoint (GET/POST): https://jsonplaceholder.typicode.com/posts/{post_id}/comments .

###### Documentação:
https://documenter.getpostman.com/view/3611046/UVXokYuz

-----

#### O QUE SERÁ AVALIADO:
Clean Code + Arquitetura MVVM + Layout

#### OBRIGATORIEDADES ANDROID:
Utilizar Kotlin.
Google Material Design.
Padrão de Arquitetura.

#### OBRIGATORIEDADES IOS:
Utilizar Swift. 
Auto Layout - ViewCode. 
Padrão de arquitetura.